/**
 * Created by Shahrukh Khan on 07/04/17.
 * email : Shahrukh@IamShahrukh.com
 */
$('.contact').click(function()
{
    var contactId = $(this).data('id');

    $.get('/contact/'+ contactId, function(data)
    {
        var contactView = $('.contact-view');
        contactView.empty();
        contactView.append(data);
    })
});