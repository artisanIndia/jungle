<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>Reactor - Bootstrap Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
    <!-- page stylesheets -->
    <link rel="stylesheet" href="/vendor/ioslist/dist/css/jquery.ioslist.css">
    <!-- end page stylesheets -->
    <!-- build:css({.tmp,app}) styles/app.min.css -->
    <link rel="stylesheet" href="/styles/webfont.css">
    <link rel="stylesheet" href="/styles/climacons-font.css">
    <link rel="stylesheet" href="/vendor/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="/styles/font-awesome.css">
    <link rel="stylesheet" href="/styles/card.css">
    <link rel="stylesheet" href="/styles/sli.css">
    <link rel="stylesheet" href="/styles/animate.css">
    <link rel="stylesheet" href="/styles/app.css">
    <link rel="stylesheet" href="/styles/app.skins.css">
    <!-- endbuild -->
</head>

<body class="page-loading">
<!-- page loading spinner -->
<div class="pageload">
    <div class="pageload-inner">
        <div class="sk-rotating-plane"></div>
    </div>
</div>
<!-- /page loading spinner -->
<div class="app layout-fixed-header">
    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">
        <div class="brand">
            <!-- toggle small sidebar menu -->
            <a href="javascript:;" class="toggle-apps hidden-xs" data-toggle="quick-launch">
                <i class="icon-grid"></i>
            </a>
            <!-- /toggle small sidebar menu -->
            <!-- toggle offscreen menu -->
            <div class="toggle-offscreen">
                <a href="javascript:;" class="visible-xs hamburger-icon" data-toggle="offscreen" data-move="ltr">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </div>
            <!-- /toggle offscreen menu -->
            <!-- logo -->
            <a class="brand-logo">
                <span>Reactor</span>
            </a>
            <a href="#" class="small-menu-visible brand-logo">R</a>
            <!-- /logo -->
        </div>
        <ul class="quick-launch-apps hide">
            <li>
                <a href="apps-gallery.html">
            <span class="app-icon bg-danger text-white">
            G
            </span>
                    <span class="app-title">Gallery</span>
                </a>
            </li>
            <li>
                <a href="apps-messages.html">
            <span class="app-icon bg-success text-white">
            M
            </span>
                    <span class="app-title">Messages</span>
                </a>
            </li>
            <li>
                <a href="apps-social.html">
            <span class="app-icon bg-primary text-white">
            S
            </span>
                    <span class="app-title">Social</span>
                </a>
            </li>
            <li>
                <a href="apps-travel.html">
            <span class="app-icon bg-info text-white">
            T
            </span>
                    <span class="app-title">Travel</span>
                </a>
            </li>
        </ul>
        <!-- main navigation -->
        <nav role="navigation">
            <ul class="nav">
                <!-- dashboard -->
                <li>
                    <a href="/home">
                        <i class="icon-compass"></i>
                        <span>Home</span>
                    </a>
                </li>
                <!-- /dashboard -->
                <!-- customizer -->
                <li>
                    <a href="/add-contact">
                        <i class="icon-equalizer"></i>
                        <span>Add Contact</span>
                    </a>
                </li>
                <!-- /customizer -->
            </ul>
        </nav>
        <!-- /main navigation -->
    </div>
    <!-- /sidebar panel -->
    @yield('content')
<!-- bottom footer -->
<footer class="content-footer">
    <nav class="footer-right">
        <ul class="nav">
            <li>
                <a href="javascript:;">Feedback</a>
            </li>
            <li>
                <a href="javascript:;" class="scroll-up">
                    <i class="fa fa-angle-up"></i>
                </a>
            </li>
        </ul>
    </nav>
    <nav class="footer-left hidden-xs">
        <ul class="nav">
            <li>
                <a href="javascript:;"><span>About</span> Reactor</a>
            </li>
            <li>
                <a href="javascript:;">Privacy</a>
            </li>
            <li>
                <a href="javascript:;">Terms</a>
            </li>
            <li>
                <a href="javascript:;">Help</a>
            </li>
        </ul>
    </nav>
</footer>
<!-- /bottom footer -->
<!-- chat -->
<div class="chat-panel">
    <div class="chat-inner">
        <div class="chat-users">
            <div class="chat-group mb0">
                <div class="chat-group-header h4">
                    Chat
                </div>
            </div>
            <div class="chat-group">
                <div class="chat-group-header">
                    Favourites
                </div>
                <a href="javascript:;">
                    <span class="status-online"></span>
                    <span>Catherine Moreno</span>
                </a>
                <a href="javascript:;">
                    <span class="status-online"></span>
                    <span>Denise Peterson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-away"></span>
                    <span>Charles Wilson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-away"></span>
                    <span>Melissa Welch</span>
                </a>
                <a href="javascript:;">
                    <span class="status-no-disturb"></span>
                    <span>Vincent Peterson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Pamela Wood</span>
                </a>
            </div>
            <div class="chat-group">
                <div class="chat-group-header">
                    Online Friends
                </div>
                <a href="javascript:;">
                    <span class="status-online"></span>
                    <span>Tammy Carpenter</span>
                </a>
                <a href="javascript:;">
                    <span class="status-away"></span>
                    <span>Emma Sullivan</span>
                </a>
                <a href="javascript:;">
                    <span class="status-no-disturb"></span>
                    <span>Andrea Brewer</span>
                </a>
                <a href="javascript:;">
                    <span class="status-online"></span>
                    <span>Sean Carpenter</span>
                </a>
            </div>
            <div class="chat-group">
                <div class="chat-group-header">
                    Offline
                </div>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Denise Peterson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Jose Rivera</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Diana Robertson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>William Fields</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Emily Stanley</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Jack Hunt</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Sharon Rice</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Mary Holland</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Diane Hughes</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Steven Smith</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Emily Henderson</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Wayne Kelly</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Jane Garcia</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Jose Jimenez</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Rachel Burton</span>
                </a>
                <a href="javascript:;">
                    <span class="status-offline"></span>
                    <span>Samantha Ruiz</span>
                </a>
            </div>
        </div>
        <div class="chat-conversation">
            <div class="chat-header">
                <a class="chat-back" href="javascript:;">
                    <i class="icon-arrow-left"></i>
                </a>
                <div class="chat-header-title">
                    Charles Wilson
                </div>
            </div>
            <div class="chat-conversation-content">
                <p class="text-center text-muted small text-uppercase bold pb15">
                    Yesterday
                </p>
                <div class="chat-conversation-user them">
                    <div class="chat-conversation-message">
                        <p>Hey.</p>
                    </div>
                </div>
                <div class="chat-conversation-user them">
                    <div class="chat-conversation-message">
                        <p>How are the wife and kids, Taylor?</p>
                    </div>
                </div>
                <div class="chat-conversation-user me">
                    <div class="chat-conversation-message">
                        <p>Pretty good, Samuel.</p>
                    </div>
                </div>
                <p class="text-center text-muted small text-uppercase bold pb15">
                    Today
                </p>
                <div class="chat-conversation-user them">
                    <div class="chat-conversation-message">
                        <p>Curabitur blandit tempus porttitor.</p>
                    </div>
                </div>
                <div class="chat-conversation-user me">
                    <div class="chat-conversation-message">
                        <p>Goodnight!</p>
                    </div>
                </div>
                <div class="chat-conversation-user them">
                    <div class="chat-conversation-message">
                        <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                    </div>
                </div>
            </div>
            <div class="chat-conversation-footer">
                <button class="chat-input-tool">
                    <i class="fa fa-camera"></i>
                </button>
                <div class="chat-input" contenteditable=""></div>
                <button class="chat-send">
                    <i class="fa fa-paper-plane"></i>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- /chat -->
</div>
<!-- build:js({.tmp,app}) scripts/app.min.js -->
<script src="/scripts/helpers/modernizr.js"></script>
<script src="/vendor/jquery/dist/jquery.js"></script>
<script src="/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/vendor/fastclick/lib/fastclick.js"></script>
<script src="/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
<script src="/scripts/helpers/smartresize.js"></script>
<script src="/scripts/constants.js"></script>
<script src="/scripts/main.js"></script>
<!-- endbuild -->
<!-- page scripts -->
<script src="/vendor/ioslist/dist/js/jquery.ioslist.js"></script>
<!-- end page scripts -->
<!-- initialize page scripts -->
<script type="text/javascript">
    $('.ioslist').ioslist();
</script>
<!-- end initialize page scripts -->

<script src="/scripts/forms/plugins.js"></script>
<script src="/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/js/function.js"></script>
</body>

</html>