<div class="scroll full-height">
    <div class="full-height">
        <div class="contact-header m-b">
            <div class="contact-toolbar visible-xs m-b">
                <a href="#">
                    <span class="icon-close visible-xs m-r m-l"></span>
                </a>
            </div>
            <div class="p-a">
                <div class="pull-left p-r">
                    <img src="images/face4.jpg" class="avatar avatar-lg img-circle" alt="">
                </div>
                <div class="overflow-hidden">
                    <h1>{{$contact->name}}</h1>
                    <h3>{{$contact->profession}}</h3>
                </div>
            </div>
        </div>
        @if($contact->email)
        <div class="row m-b">
            <div class="col-xs-4 text-right">
                <span class="text-muted">email</span>
            </div>
            <div class="col-xs-8">
                <a href="email@example.com">{{$contact->email}}</a>
            </div>
        </div>
        @endif
        <div class="row m-b">
            <div class="col-xs-4 text-right">
                <span class="text-muted">mobile</span>
            </div>
            <div class="col-xs-8">
                <span>{{$contact->phone}}</span>
            </div>
        </div>
        @if($contact->home)
        <div class="row m-b">
            <div class="col-xs-4 text-right">
                <span class="text-muted">home</span>
            </div>
            <div class="col-xs-8">
                <a href="http://www.example.com">{{$contact->home}}</a>
            </div>
        </div>
        @endif
        @if($contact->country)
        <div class="row m-b">
            <div class="col-xs-4 text-right">
                <span class="text-muted">country</span>
            </div>
            <div class="col-xs-8">
                <span>{{$contact->country}}</span>
            </div>
        </div>
        @endif
        @if($contact->note)
        <div class="row m-b">
            <div class="col-xs-4 text-right">
                <span class="text-muted">note</span>
            </div>
            <div class="col-xs-8">
                <span>{{$contact->note}}</span>
            </div>
        </div>
        @endif
        <a href="/edit-contact/{{$contact->id}}"><button type="button" class="btn btn-success">Edit</button></a>
        <a href="/export-contact/{{$contact->id}}"><button type="button" class="btn btn-success">Download</button></a>
    </div>
</div>