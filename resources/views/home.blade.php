@extends('layouts.app')
@section('content')
    <!-- content panel -->
    <div class="main-panel">
        @include('layouts.header')
        <!-- main area -->
        <div class="main-content no-padding">
            <div class="fill-container">
                <div class="relative full-height">
                    <div class="display-columns">
                        <div class="column contacts-sidebar hidden-xs bg-white b-r">
                            <div class="scroll">
                                <div class="p-a">
                                    <nav role="navigation">
                                        <a href="javascript:;" class="btn btn-default btn-block m-b">You have a total of {{$count}} contact{{ ($count > 1) ? 's' : ''}}</a>
                                        <ul class="nav nav-stacked nav-pills">
                                            <li class="disabled text-uppercase small">
                                                <a href="javascript:;">
                                                    <strong>Filter by Tags</strong>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="icon-tag"></i>
                                                    <span>All tags</span>
                                                </a>
                                            </li>
                                            @foreach(App\Models\Tag::all() as $tag)
                                            <li>
                                                <a href="?tag={{$tag->name}}">
                                                    <i class="icon-tag text-primary"></i>
                                                    <span>{{$tag->name}}</span>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="column contacts-list bg-white b-r">
                            <div class="ioslist">
                                @foreach($sortedContacts as $key => $sorted)
                                <div class="ioslist-group-container">
                                    <div class="ioslist-group-header">{{$key}}</div>
                                    @foreach($sorted as $contact)
                                    <ul>
                                        <li class="contact" data-id="{{$contact->id}}">{{$contact->name}}</li>
                                    </ul>
                                        @endforeach
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="column contact-view">
                            <!-- get the data for this tab from ajax -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /main area -->
    </div>
    <!-- /content panel -->
@endsection