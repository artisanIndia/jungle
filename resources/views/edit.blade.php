@extends('layouts.app')

@section('content')
    <div class="main-panel">
        @include('layouts.header')

        <div class="main-content">
            <div class="card bg-white">
                <div class="card-header">
                    Add a contact
                </div>
                <div class="card-block">
                    <div class="row m-a-0">
                        <div class="col-lg-12">
                            <form class="form-horizontal" role="form" method="POST">
                                {{csrf_field()}}
                                @if(session('contact_saved'))
                                    <div class="alert alert-success">
                                        {{session('contact_saved')}}
                                    </div>
                                @endif
                                <div class="form-group {{ $errors->first('name') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" value="{{$contact->name}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('name') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" value="{{$contact->email}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('email') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('phone') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Phone</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="phone" class="form-control" value="{{$contact->phone}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('phone') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('address') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" rows="3">{{$contact->address}}</textarea>
                                    </div>
                                    <p class="help-block">{{ $errors->first('address') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('country') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="country" class="form-control" value="{{$contact->country}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('country') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('profession') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Profession</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="profession" class="form-control" value="{{$contact->profession}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('profession') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('company') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Company</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="company" class="form-control" value="{{$contact->company}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('company') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('birthday') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Date of Birth</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control m-b" name="birthday" data-provide="datepicker"
                                               placeholder="Datepicker" value="{{date('m/d/Y', strtotime($contact->birthday))}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('birthday') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('home') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Website</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="home" class="form-control" value="{{$contact->home}}">
                                    </div>
                                    <p class="help-block">{{ $errors->first('home') }}</p>
                                </div>

                                <div class="form-group {{ $errors->first('note') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">Note</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="note" rows="3">{{$contact->note}}</textarea>
                                    </div>
                                    <p class="help-block">{{ $errors->first('note') }}</p>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Type</label>
                                    <div class="col-sm-10">
                                        {{ Form::select('tag_id', $tags, $contact->tag_id, ['class' => 'form-control']) }}
                                    </div>
                                </div>

                                <div class="form-group pull-right">
                                    <input type="submit" class="btn btn-success" value="Save">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /main area -->
    </div>
@endsection