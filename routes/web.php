<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'ContactController@index');

    Route::get('/contact/{id}', 'ContactController@getSingleContact');

    Route::get('add-contact', 'ContactController@addContact');

    Route::post('add-contact', 'ContactController@storeContact');

    Route::get('export-contact/{contactId}', 'ContactController@exportVCard');

    Route::get('edit-contact/{id}', 'ContactController@editContact');

    Route::post('edit-contact/{id}', 'ContactController@updateContact');
});