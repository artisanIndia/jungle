<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Models\Contact;
use JeroenDesloovere\VCard\VCard;


/**
 * Class ContactController
 * @package App\Http\Controllers
 */
class ContactController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $contacts = Contact::query();
        if($request->get('query'))
        {
            $query = $request->get('query');
            $contacts = $contacts->whereRaw("name like '%$query%'");
        }
        if($request->get('tag'))
        {
            $tag = $request->get('tag');
            $tagId = Tag::where('name', $tag)->pluck('id')->first();

            if($tagId)
            {
                $contacts = $contacts->where('tag_id', $tagId);
            }
        }

        $contacts = $contacts->orderBy('name')->get();

        $sortedContacts = [];
        foreach($contacts as $contact)
        {
            $letter = substr($contact->name, 0, 1);
            $sortedContacts[strtolower($letter)][] = $contact;
        }
        $count = Contact::count();
        return view('home', compact('sortedContacts', 'request', 'count'));

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getSingleContact($id)
    {
        $contact = Contact::where(['id' => $id, 'owner_id' => \Auth::user()->id])->first();
        if($contact)
        {
            $html = view('contact', compact('contact'));
            $html = $html->render();
            return $html;
        }
        else
        {
            $html = view('unauthorized');
            $html = $html->render();
            return $html;
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addContact()
    {
        $tags = Tag::get();

        return view('add-contact', compact('tags'));
    }

    /**
     * @param ContactRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeContact(ContactRequest $request)
    {
        $birthday = date('Y-m-d', strtotime($request->birthday));

        $model = new Contact();
        $model->fill(array_except($request->all(), '_token', 'birthday'));
        $model->birthday = $birthday;
        $model->owner_id = \Auth::user()->id;
        $model->save();
        \Session::flash('contact_saved', 'Contact saved successfully');
        return redirect()->back();
    }

    /**
     * @param $contactId
     */
    public function exportVCard($contactId)
    {
        $contact = Contact::where(['id' => $contactId, 'owner_id' => \Auth::user()->id])->first();
        if($contact)
        {
            $vcard = new VCard();
            $vcard->addName($contact->name);
            $vcard->addEmail($contact->email);
            $vcard->addAddress($contact->address, $contact->country);
            $vcard->addBirthday($contact->birthday);
            $vcard->addURL($contact->home);
            $vcard->addNote($contact->note);
            $vcard->addCompany($contact->company, $contact->profession);

            return $vcard->download();
        }
    }

    /**
     * @param $contactId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editContact($contactId)
    {
        $tagModel = Tag::get();
        $tags = [];
        foreach($tagModel as $tag)
        {
            $tags[$tag->id] = $tag->name;
        }

        $contact = Contact::where('owner_id', \Auth::user()->id)->findOrFail($contactId);
        return view('edit', compact('contact', 'tags'));
    }

    /**
     * @param ContactRequest $request
     * @param $contactId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateContact(ContactRequest $request, $contactId)
    {
        $contact = Contact::where(['id' => $contactId, 'owner_id' => \Auth::user()->id])->firstOrFail();
        $birthday = date('Y-m-d', strtotime($request->birthday));
        $contact->fill(array_except($request->all(), '_token', 'birthday'));
        $contact->birthday = $birthday;
        $contact->save();
        \Session::flash('contact_saved', 'Contact saved successfully');
        return redirect()->back();

    }
}
